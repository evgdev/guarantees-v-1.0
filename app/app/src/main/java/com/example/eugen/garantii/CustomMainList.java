package com.example.eugen.garantii;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


public class CustomMainList extends ArrayAdapter<String> {

  private final Context context;
  private final String[] clientName, clientPhone, clientAddress, orderIndex, clientDistance, serialNumber;
  public View view;


  public CustomMainList(Context context, String[] clientName, String[] clientPhone, String[] clientAddress, String[] orderIndex, String[] clientDistance, String[] serialNumber) {
    super(context, -1, clientName);
    this.context = context;
    this.clientName = clientName;
    this.clientPhone = clientPhone;
    this.clientAddress = clientAddress;
    this.orderIndex = orderIndex;
    this.clientDistance = clientDistance;
    this.serialNumber = serialNumber;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {

    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    view = inflater.inflate(R.layout.list_view_item, parent, false);
    TextView Name = view.findViewById(R.id.clientName);
    TextView Phone = view.findViewById(R.id.clientPhone);
    TextView Address = view.findViewById(R.id.clientAddress);
    TextView Distance = view.findViewById(R.id.clientDistance);
    TextView Serial = view.findViewById(R.id.serialNumber);
    Name.setText(clientName[position]);
    Name.setTag(orderIndex[position]);
    Phone.setText(clientPhone[position]);
    Address.setText(clientAddress[position]);
    Distance.setText(clientDistance[position]);
    Serial.setText(serialNumber[position]);

    return view;
  }
}
