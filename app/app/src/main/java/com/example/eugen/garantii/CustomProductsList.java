package com.example.eugen.garantii;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CustomProductsList extends ArrayAdapter<String> {

  private final Context context;
  private final String[] productID, denProduct, countProduct;
  public View view;


  public CustomProductsList(Context context, String[] productID, String[] denProduct, String[] countProduct) {
    super(context,-1, productID);

    this.context = context;
    this.productID = productID;
    this.denProduct = denProduct;
    this.countProduct = countProduct;

  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    view = inflater.inflate(R.layout.custom_products_list, parent, false);

    TextView id = view.findViewById(R.id.productID);
    TextView den = view.findViewById(R.id.denProduct);
    TextView count = view.findViewById(R.id.contProduct);



    id.setText(productID[position]);
    den.setText(denProduct[position]);
    count.setText(countProduct[position]);

    return view;
  }

}
