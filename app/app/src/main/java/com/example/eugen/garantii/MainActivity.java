package com.example.eugen.garantii;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.List;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
  LocationManager locationManager;
  LocationListener locationListener;

  ListView listOrders;
  String clientName, clientPhone, clientAddress, clientDistance, clientLatitude, clientLongitude, serialNumber, phone, address;
  JSONObject orders;
  ProgressDialog loading;


  public double myLongitude;
  public double myLatitude;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    loading = new ProgressDialog(MainActivity.this);
    loading.setMessage("Loading orders. Please wait");
    loading.setCancelable(false);
    loading.show();

    listOrders = findViewById(R.id.listOrders);

    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

    locationListener = new LocationListener() {

      @Override
      public void onLocationChanged(Location location) {
        myLongitude = location.getLongitude();
        myLatitude = location.getLatitude();

        final Context context = MainActivity.this.getApplicationContext();
        List<SubscriptionInfo> subscriptionInfos = SubscriptionManager.from(context).getActiveSubscriptionInfoList();
        if (subscriptionInfos == null || subscriptionInfos.isEmpty()) {
          Toast.makeText(context, "No sims installed!", Toast.LENGTH_SHORT).show();
        } else {
          for (SubscriptionInfo subscriptionInfo : subscriptionInfos) {

            final String simSerialNumber = subscriptionInfo.getIccId();
            String url = getString(R.string.api_url) + simSerialNumber + "&lat=" + myLatitude + "&long=" + myLongitude;
            NetworkGetRequest networkGetRequest = new NetworkGetRequest();
            networkGetRequest.run(url, new Callback() {
              @Override
              public void onFailure(Call call, IOException e) {
                e.printStackTrace();
              }

              @Override
              public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                  throw new IOException("Unexpected code" + response);
                }
                final String responseData = response.body().string();
                MainActivity.this.runOnUiThread(new Runnable() {
                  @Override
                  public void run() {
                    try {
                      JSONObject resp = new JSONObject(responseData);
                      if (resp.length() == 0) {
                        //Login failed
                        Toast.makeText(MainActivity.this, "Login Failed!", Toast.LENGTH_LONG).show();
                      } else {
                        //Login succeded
                        JSONObject user = resp.getJSONObject("user");
                        String nume = user.getString("nume");
                        String prenume = user.getString("prenume");
                        Toast.makeText(MainActivity.this, "Bine ati venit " + nume + " " + prenume, Toast.LENGTH_SHORT).show();

                        orders = resp.getJSONObject("zaeavki");
                        String[] clientsName = new String[orders.length()];
                        String[] clientsPhones = new String[orders.length()];
                        String[] clientsAddress = new String[orders.length()];
                        String[] clientsDistance = new String[orders.length()];
                        String [] orderIndex = new String[orders.length()];
                        String[] serialNumbers = new String[orders.length()];
                        for (int i = 0; i < orders.length(); i++) {

                          final JSONObject order = orders.getJSONObject(String.valueOf(i));
                          clientName = order.getString("client_name");
                          clientAddress = order.getString("location_str");
                          clientPhone = order.getString("contact_nr");
                          clientLatitude = order.getString("lat");
                          clientLongitude = order.getString("long");
                          clientDistance = order.getString("distance");
                          serialNumber = order.getString("shasi");

                          clientsName[i] = clientName;
                          clientsPhones[i] = clientPhone;
                          clientsAddress[i] = clientAddress;
                          clientsDistance[i] = clientDistance;
                          orderIndex[i] = String.valueOf(i);
                          serialNumbers[i] = serialNumber;
                        }
                        CustomMainList listItem = new CustomMainList(MainActivity.this, clientsName, clientsPhones, clientsAddress, orderIndex, clientsDistance, serialNumbers);
                        listOrders.setAdapter(listItem);


                        if (loading.isShowing()){
                            loading.dismiss();
                        }
                      }
                    } catch (JSONException e) {
                      e.printStackTrace();
                    }
                  }
                });
              }
            });
          }
        }
      }

      @Override
      public void onStatusChanged(String provider, int status, Bundle extras) {

      }

      @Override
      public void onProviderEnabled(String provider) {

      }

      @Override
      public void onProviderDisabled(String provider) {

      }
    };
    locationManager.requestLocationUpdates(locationManager.GPS_PROVIDER,5*60*1000,0,locationListener);

    listOrders.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

        TextView textView = view.findViewById(R.id.clientPhone);
        TextView name = view.findViewById(R.id.clientName);
        TextView addresstext = view.findViewById(R.id.clientAddress);
        TextView serialInfo = view.findViewById(R.id.serialNumber);

        serialNumber = serialInfo.getText().toString();
        phone = textView.getText().toString();
        address = addresstext.getText().toString();


        final String orderIndex = name.getTag().toString();
        try {
          final JSONObject order = orders.getJSONObject(orderIndex);
          final AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
          View v = getLayoutInflater().inflate(R.layout.custom_main_alert, null);
          Button call = v.findViewById(R.id.call);
          Button map = v.findViewById(R.id.map);
          Button info = v.findViewById(R.id.info);
          alert.setCancelable(true);
          alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
          });
          alert.setView(v);
          alert.show();

          call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              if (checkPermission(Manifest.permission.CALL_PHONE)) {
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone)));
              } else {
                Toast.makeText(MainActivity.this, "Permission Call Phone denied", Toast.LENGTH_SHORT).show();
              }
            }
          });

          map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              try {
                clientLatitude = order.getString("lat");
                clientLongitude = order.getString("long");
              } catch (JSONException e) {
                e.printStackTrace();
              }
              Uri gmmIntentUri = Uri.parse("http://maps.google.com/maps?saddr="+myLatitude+","+myLongitude+"&daddr="+clientLatitude+","+clientLongitude+"?z=12");//Uri.parse("geo:"+clientLatitude+","+clientLongitude+"?z=12&q"+address);//. + clientLatitude + "," + clientLongitude + "("+ address + ")");
              Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
              mapIntent.setPackage("com.google.android.apps.maps");
              startActivity(mapIntent);
            }
          });
          info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Intent info = new Intent(MainActivity.this, OrderActivity.class);
              info.putExtra("serialInfo", serialNumber);
              startActivity(info);
            }
          });
          if (checkPermission(Manifest.permission.CALL_PHONE)) {
          } else {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CALL_PHONE}, 1);
          }

        } catch (JSONException e) {
          e.printStackTrace();
        }
      }
    });

  }

  private boolean checkPermission(String permission) {
    return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
  }
  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    switch(requestCode) {
      case 1 :
        if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
          Toast.makeText(this, "You can call the number by clicking on the button", Toast.LENGTH_SHORT).show();
        }
        return;
    }
  }
}
