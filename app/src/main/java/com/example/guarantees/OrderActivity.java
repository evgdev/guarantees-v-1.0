package com.example.guarantees;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OrderActivity extends AppCompatActivity {

  Button addProducts, addServices, addPhoto, addVideo;
  ImageButton notice;
  TextView idOreder, techName, serialNumber, orderDesc, noticeTextView;
  ListView noticeList;
  ArrayAdapter<String> noticeAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_order);

    idOreder = findViewById(R.id.idOrder);
    techName = findViewById(R.id.techName);
    serialNumber = findViewById(R.id.serialNumber);
    orderDesc = findViewById(R.id.orderDesc);
    orderDesc.setMovementMethod(ScrollingMovementMethod.getInstance());

    addProducts = findViewById(R.id.addProducts);
    addServices = findViewById(R.id.addServices);
    addPhoto = findViewById(R.id.addPhoto);
    addVideo = findViewById(R.id.addVideo);

    notice = findViewById(R.id.noticeIcon);
    noticeList = findViewById(R.id.noticeList);
    //noticeTextView = findViewById(R.id.notice);
    //noticeTextView.setMovementMethod(ScrollingMovementMethod.getInstance());


    noticeAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, new ArrayList<String>());
    noticeList.setAdapter(noticeAdapter);

    Bundle mainActivityExtra = getIntent().getExtras();
    idOreder.setText(mainActivityExtra.getString("orderID"));
    techName.setText(mainActivityExtra.getString("techName"));
    serialNumber.setText(mainActivityExtra.getString("serial"));

    notice.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        final AlertDialog.Builder noticeDialog = new AlertDialog.Builder(OrderActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View noticeDialogView = inflater.inflate(R.layout.custom_notice_alert, null);
        noticeDialog.setView(noticeDialogView);
        final EditText noticeText = noticeDialogView.findViewById(R.id.noticeText);
        Button addNotice = noticeDialogView.findViewById(R.id.addNotice);
        Button deleteAll = noticeDialogView.findViewById(R.id.deleteAll);
        noticeDialog.setCancelable(true);
        final AlertDialog dialog = noticeDialog.create();
        addNotice.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            String noticeMessage = noticeText.getText().toString();
            if (noticeMessage.length() > 0) {
              noticeAdapter.add(noticeMessage);
              noticeAdapter.notifyDataSetChanged();
              if (dialog.isShowing()){
                dialog.dismiss();
              }
            } else {
              Toast.makeText(OrderActivity.this, "Add Notice", Toast.LENGTH_LONG).show();
            }
            if (noticeList.getAdapter().getCount() > 0) {
              final int version = Build.VERSION.SDK_INT;
              if (version >= 21) {
                notice.setBackground(ContextCompat.getDrawable(OrderActivity.this, R.drawable.star_active));
              } else {
                notice.setBackground(ContextCompat.getDrawable(OrderActivity.this, R.drawable.star));
              }
            }
          }
        });
        deleteAll.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            noticeList.setAdapter(null);
            if (dialog.isShowing()){
              dialog.dismiss();
            }
            notice.setBackground(ContextCompat.getDrawable(OrderActivity.this, R.drawable.star));
          }
        });

        dialog.show();
      }
    });
    noticeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        View view1 = getLayoutInflater().inflate(R.layout.custom_item_notice_edit, null);
        final AlertDialog.Builder dialog = new AlertDialog.Builder(OrderActivity.this);

        dialog.setCancelable(true);

        final EditText editNotice = view1.findViewById(R.id.editNotice);
        Button deleteNotice = view1.findViewById(R.id.deleteNotice);
        Button editNoticeBtn = view1.findViewById(R.id.editNoticeBtn);

        dialog.setView(view1);
        final AlertDialog alertDialog = dialog.create();
        alertDialog.show();

        deleteNotice.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            int count = noticeAdapter.getCount();
            for (int i = 0; i < count; i++){
              noticeAdapter.remove(noticeAdapter.getItem(i));
            }
            notice.setBackground(ContextCompat.getDrawable(OrderActivity.this, R.drawable.star));
            if (alertDialog.isShowing()){
              alertDialog.dismiss();
            }
          }
        });
        editNoticeBtn.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            editNotice.setVisibility(View.VISIBLE);
            String noticeString = noticeList.getItemAtPosition(position).toString();
            editNotice.setText(noticeString);
            editNotice.requestFocus();
            InputMethodManager mm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            mm.showSoftInput(editNotice, InputMethodManager.SHOW_IMPLICIT);
          }
        });
      }
    });

    addPhoto.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent addPhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(addPhoto, 1);
      }
    });
    addVideo.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent addVideo = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        addVideo.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
        startActivityForResult(addVideo, 1);
      }
    });
    addProducts.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        AlertDialog.Builder products = new AlertDialog.Builder(OrderActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View prod = inflater.inflate(R.layout.custom_order_alert, null);
        products.setView(prod);
        ListView listProducts = prod.findViewById(R.id.listProducts);

        try {
          if (((ApplicationVars)getApplication()).getJSONArrayProducts().length() == 0){
            Toast.makeText(OrderActivity.this, "Data didn't load", Toast.LENGTH_SHORT).show();
          } else {

            String[] productsID = new String[((ApplicationVars)getApplication()).getJSONArrayProducts().length()];
            String[] denProducts = new String[((ApplicationVars)getApplication()).getJSONArrayProducts().length()];
            String[] countProducts = new String[((ApplicationVars)getApplication()).getJSONArrayProducts().length()];
            String[] priceProducts = new String[((ApplicationVars)getApplication()).getJSONArrayProducts().length()];

            for (int i = 0; i < ((ApplicationVars)getApplication()).getJSONArrayProducts().length(); i++){

              final JSONObject resp = ((ApplicationVars)getApplication()).getJSONArrayProducts().getJSONObject(i);

              ((ApplicationVars)getApplication()).setProductID(resp.getString("Cod Marfa"));
              ((ApplicationVars)getApplication()).setDenProduct(resp.getString("Denumire marfa"));
              ((ApplicationVars)getApplication()).setCountProduct(resp.getString("cantitate marfa"));
              ((ApplicationVars)getApplication()).setPriceProduct(resp.getString("pret marfa"));

              productsID[i] = ((ApplicationVars)getApplication()).getProductID();
              denProducts[i] = ((ApplicationVars)getApplication()).getDenProduct();
              countProducts[i] = ((ApplicationVars)getApplication()).getCountProduct();
              priceProducts[i] = ((ApplicationVars)getApplication()).getPriceProduct();
            }
            CustomProductsList listitem = new CustomProductsList(OrderActivity.this, productsID, denProducts, countProducts, priceProducts);
            listProducts.setAdapter(listitem);
          }
        } catch (JSONException e) {
          e.printStackTrace();
        }
        products.create();
        products.show();
      }
    });

  }
}
