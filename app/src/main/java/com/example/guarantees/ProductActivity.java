package com.example.guarantees;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class ProductActivity extends AppCompatActivity {

  String productID, denProduct, countProduct, priceProduct;
  ListView listProducts;
  Button addProducts, addServices;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_product);

    listProducts = findViewById(R.id.listProducts);
    addProducts = findViewById(R.id.addProducts);
    addServices = findViewById(R.id.addServices);

    String url = "http://eugen.agro.md/api/return-products.php";
    NetworkGetRequest request = new NetworkGetRequest();
    request.run(url, new Callback() {
      @Override
      public void onFailure(Call call, IOException e) {
        e.printStackTrace();
      }

      @Override
      public void onResponse(Call call, Response response) throws IOException {
        if (!response.isSuccessful()) {
          throw new IOException("Unexpected code" + response);
        }
        final String responseData = response.body().string();
        ProductActivity.this.runOnUiThread(new Runnable() {
          @Override
          public void run() {
            try {
              JSONArray response = new JSONArray(responseData);
              if (response.length() == 0){
                Toast.makeText(ProductActivity.this, "Data didn't load", Toast.LENGTH_SHORT).show();
              } else {

                String[] productsID = new String[response.length()];
                String[] denProducts = new String[response.length()];
                String[] countProducts = new String[response.length()];
                String[] priceProducts = new String[response.length()];

                for (int i = 0; i < response.length(); i++){

                  final JSONObject resp = response.getJSONObject(i);

                  productID = resp.getString("Cod Marfa");
                  denProduct = resp.getString("Denumire marfa");
                  countProduct = resp.getString("cantitate marfa");
                  priceProduct = resp.getString("pret marfa");

                  productsID[i] = productID;
                  denProducts[i] = denProduct;
                  countProducts[i] = countProduct;
                  priceProducts[i] = priceProduct;
                }
                CustomProductsList listitem = new CustomProductsList(ProductActivity.this, productsID, denProducts, countProducts, priceProducts);
                listProducts.setAdapter(listitem);

              }
            } catch (JSONException e) {
              e.printStackTrace();
            }
          }
        });
      }
    });
    addProducts.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

      }
    });
  }
  public void productIDClick(View v){
    TextView tv = v.findViewById(R.id.productID);
    AlertDialog.Builder alert = new AlertDialog.Builder(ProductActivity.this);
    alert.setMessage(tv.getText()).setCancelable(true);
    alert.setNegativeButton("OK", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    alert.show();
  }
  public void denProductClick(View v){
    TextView tv = v.findViewById(R.id.denProduct);
    AlertDialog.Builder alert = new AlertDialog.Builder(ProductActivity.this);
    alert.setMessage(tv.getText()).setCancelable(true);
    alert.setNegativeButton("OK", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    alert.show();
  }
}
