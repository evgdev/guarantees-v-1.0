package com.example.guarantees;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ProductInfoList extends ArrayAdapter<String> {
  private final Context  context;
  private final String[] ordersID, ordersDate, ordersType;
  public View view;

  public ProductInfoList(Context context, String[] ordersID, String[] ordersDate, String[] ordersType) {
    super(context, -1, ordersID);
    this.context = context;
    this.ordersID = ordersID;
    this.ordersDate = ordersDate;
    this.ordersType = ordersType;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent){
    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    view = inflater.inflate(R.layout.product_info_list_item, parent, false);
    TextView ordersIDText = view.findViewById(R.id.techOrder);
    TextView dateOrdersText = view.findViewById(R.id.techOrderDate);
    TextView ordersTypeText = view.findViewById(R.id.techOrderName);

    ordersIDText.setText(ordersID[position]);
    dateOrdersText.setText(ordersDate[position]);
    ordersTypeText.setText(ordersType[position]);
    return view;
  }
}
