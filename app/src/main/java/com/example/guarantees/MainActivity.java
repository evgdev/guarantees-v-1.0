package com.example.guarantees;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
    LocationManager locationManager;
    LocationListener locationListener;

    ListView listOrders;
    String clientName, clientPhone, clientAddress, clientDistance, clientLatitude, clientLongitude,
            serialNumber, phone, address, orderID, techName, orderDate, orderName, nume, prenume;
    JSONObject orders;
    ProgressDialog loading;
    Toolbar toolbar;
    Button user;


    public double myLongitude;
    public double myLatitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set Toolbar

        toolbar = findViewById(R.id.toolbar);
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        setSupportActionBar(toolbar);
      }
      getSupportActionBar().setDisplayShowTitleEnabled(false);

      // Loading

        loading = new ProgressDialog(MainActivity.this);
        loading.setMessage("Loading orders. Please wait");
        loading.setCancelable(false);
        loading.show();

        // Return User Products

        String url = "http://eugen.agro.md/api/return-products.php";
        NetworkGetRequest request = new NetworkGetRequest();
        request.run(url, new Callback() {
          @Override
          public void onFailure(Call call, IOException e) {
            e.printStackTrace();
          }

          @Override
          public void onResponse(Call call, Response response) throws IOException {
            if (!response.isSuccessful()) {
              throw new IOException("Unexpected code" + response);
            }
            final String responseData = response.body().string();
            MainActivity.this.runOnUiThread(new Runnable() {
              @Override
              public void run() {
                try {
                  ((ApplicationVars)getApplication()).setJSONArrayProducts(new JSONArray(responseData));
                } catch (JSONException e) {
                  e.printStackTrace();
                }
              }
            });
          }
        });

      // Return User Products END

      // Return User Orders

      locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

      locationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
          myLongitude = location.getLongitude();
          myLatitude = location.getLatitude();

          final Context context = MainActivity.this.getApplicationContext();
          List<SubscriptionInfo> subscriptionInfos = SubscriptionManager.from(context).getActiveSubscriptionInfoList();
          if (subscriptionInfos == null || subscriptionInfos.isEmpty()) {
            Toast.makeText(context, "No sims installed!", Toast.LENGTH_SHORT).show();
          } else {
            for (SubscriptionInfo subscriptionInfo : subscriptionInfos) {
              final String simSerialNumber = subscriptionInfo.getIccId();
              if (((ApplicationVars)getApplication()).allStringsNull() == true) {
                String url = getString(R.string.api_url) + simSerialNumber + "&lat=" + myLatitude + "&long=" + myLongitude;
                NetworkGetRequest networkGetRequest = new NetworkGetRequest();
                networkGetRequest.run(url, new Callback() {
                  @Override
                  public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                  }

                  @Override
                  public void onResponse(Call call, Response response) throws IOException {
                    if (!response.isSuccessful()) {
                      throw new IOException("Unexpected code" + response);
                    }
                    final String responseData = response.body().string();
                    MainActivity.this.runOnUiThread(new Runnable() {
                      @Override
                      public void run() {
                        try {
                          ((ApplicationVars) getApplication()).setJSONObjectOrders(new JSONObject(responseData));
                          if (((ApplicationVars) getApplication()).getJSONObjectOrders().length() == 0) {
                            //Login failed
                            Toast.makeText(MainActivity.this, "Login Failed!", Toast.LENGTH_LONG).show();
                          } else {
                            //Login succeded
                            JSONObject user = ((ApplicationVars) getApplication()).getJSONObjectOrders().getJSONObject("user");
                            ((ApplicationVars) getApplication()).setFirstName(user.getString("nume"));
                            ((ApplicationVars) getApplication()).setLastName(user.getString("prenume"));

                            orders = ((ApplicationVars) getApplication()).getJSONObjectOrders().getJSONObject("zaeavki");
                            String[] ordersID = new String[orders.length()];
                            String[] ordersDate = new String[orders.length()];
                            String[] ordersName = new String[orders.length()];
                            String[] clientsName = new String[orders.length()];
                            String[] clientsPhones = new String[orders.length()];
                            String[] techNames = new String[orders.length()];
                            String[] serialNumbers = new String[orders.length()];
                            String[] clientsAddress = new String[orders.length()];
                            String[] clientsDistance = new String[orders.length()];
                            for (int i = 0; i < orders.length(); i++) {

                              final JSONObject order = orders.getJSONObject(String.valueOf(i));
                              ((ApplicationVars) getApplication()).setOrderID(order.getString("id"));
                              ((ApplicationVars) getApplication()).setOrderDate(order.getString("data"));
                              ((ApplicationVars) getApplication()).setOrderName(order.getString("tip order"));
                              ((ApplicationVars) getApplication()).setClientName(order.getString("client_name"));
                              ((ApplicationVars) getApplication()).setClientPhone(order.getString("contact_nr"));
                              ((ApplicationVars) getApplication()).setClientLatitude(order.getString("lat"));
                              ((ApplicationVars) getApplication()).setClientLongitude(order.getString("long"));
                              ((ApplicationVars) getApplication()).setTechName(order.getString("tehnica"));
                              ((ApplicationVars) getApplication()).setSerialNumber(order.getString("shasi"));
                              ((ApplicationVars) getApplication()).setClientDistance(order.getString("distance"));
                              ((ApplicationVars) getApplication()).setClientDistance(order.getString("location_str"));

                              ordersID[i] = ((ApplicationVars) getApplication()).getOrderID();
                              ordersDate[i] = ((ApplicationVars) getApplication()).getOrderDate();
                              ordersName[i] = ((ApplicationVars) getApplication()).getOrderName();
                              clientsName[i] = ((ApplicationVars) getApplication()).getclientName();
                              clientsPhones[i] = ((ApplicationVars) getApplication()).getClientPhone();
                              techNames[i] = ((ApplicationVars) getApplication()).getTechName();
                              serialNumbers[i] = ((ApplicationVars) getApplication()).getSerialNumber();
                              clientsAddress[i] = ((ApplicationVars) getApplication()).getClientAddress();
                              clientsDistance[i] = ((ApplicationVars) getApplication()).getClientDistance();
                            }
                            CustomMainList listItem = new CustomMainList(MainActivity.this, clientsName, clientsPhones, clientsAddress, clientsDistance, serialNumbers, ordersID, ordersDate, ordersName, techNames);
                            listOrders.setAdapter(listItem);


                            if (loading.isShowing()) {
                              loading.dismiss();
                              Toast.makeText(MainActivity.this, "Bine ati venit " + ((ApplicationVars)getApplication()).getFirstName() + " " + ((ApplicationVars)getApplication()).getLastName(), Toast.LENGTH_SHORT).show();
                            }
                          }
                        } catch (JSONException e) {
                          e.printStackTrace();
                        }
                      }
                    });
                  }
                });
              } else {
                if (loading.isShowing()){
                  loading.dismiss();
                }
                try {
                  orders = ((ApplicationVars) getApplication()).getJSONObjectOrders().getJSONObject("zaeavki");
                  String[] ordersID = new String[orders.length()];
                  String[] ordersDate = new String[orders.length()];
                  String[] ordersName = new String[orders.length()];
                  String[] clientsName = new String[orders.length()];
                  String[] clientsPhones = new String[orders.length()];
                  String[] techNames = new String[orders.length()];
                  String[] serialNumbers = new String[orders.length()];
                  String[] clientsAddress = new String[orders.length()];
                  String[] clientsDistance = new String[orders.length()];
                  for (int i = 0; i < orders.length(); i++) {

                    final JSONObject order = orders.getJSONObject(String.valueOf(i));
                    ((ApplicationVars) getApplication()).setOrderID(order.getString("id"));
                    ((ApplicationVars) getApplication()).setOrderDate(order.getString("data"));
                    ((ApplicationVars) getApplication()).setOrderName(order.getString("tip order"));
                    ((ApplicationVars) getApplication()).setClientName(order.getString("client_name"));
                    ((ApplicationVars) getApplication()).setClientPhone(order.getString("contact_nr"));
                    ((ApplicationVars) getApplication()).setClientLatitude(order.getString("lat"));
                    ((ApplicationVars) getApplication()).setClientLongitude(order.getString("long"));
                    ((ApplicationVars) getApplication()).setTechName(order.getString("tehnica"));
                    ((ApplicationVars) getApplication()).setSerialNumber(order.getString("shasi"));
                    ((ApplicationVars) getApplication()).setClientDistance(order.getString("distance"));
                    ((ApplicationVars) getApplication()).setClientDistance(order.getString("location_str"));

                    ordersID[i] = ((ApplicationVars) getApplication()).getOrderID();
                    ordersDate[i] = ((ApplicationVars) getApplication()).getOrderDate();
                    ordersName[i] = ((ApplicationVars) getApplication()).getOrderName();
                    clientsName[i] = ((ApplicationVars) getApplication()).getclientName();
                    clientsPhones[i] = ((ApplicationVars) getApplication()).getClientPhone();
                    techNames[i] = ((ApplicationVars) getApplication()).getTechName();
                    serialNumbers[i] = ((ApplicationVars) getApplication()).getSerialNumber();
                    clientsAddress[i] = ((ApplicationVars) getApplication()).getClientAddress();
                    clientsDistance[i] = ((ApplicationVars) getApplication()).getClientDistance();
                  }
                  CustomMainList listItem = new CustomMainList(MainActivity.this, clientsName, clientsPhones, clientsAddress, clientsDistance, serialNumbers, ordersID, ordersDate, ordersName, techNames);
                  listOrders.setAdapter(listItem);
                } catch (JSONException e){
                  e.printStackTrace();
                }
              }
            }
          }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
      };
      locationManager.requestLocationUpdates(locationManager.NETWORK_PROVIDER, 5 * 60 * 1000, 0, locationListener);

      // Added Orders to SwipeRefreshLayout

        final SwipeRefreshLayout swipeRefreshLayout = findViewById(R.id.listLayout);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
          @Override
          public void onRefresh() {

            // Location Listener

            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            locationListener = new LocationListener() {

              @Override
              public void onLocationChanged(Location location) {
                myLongitude = location.getLongitude();
                myLatitude = location.getLatitude();

                // Get Sim Serial Number

                final Context context = MainActivity.this.getApplicationContext();
                List<SubscriptionInfo> subscriptionInfos = SubscriptionManager.from(context).getActiveSubscriptionInfoList();
                if (subscriptionInfos == null || subscriptionInfos.isEmpty()) {
                  Toast.makeText(context, "No sims installed!", Toast.LENGTH_SHORT).show();
                } else {
                  for (SubscriptionInfo subscriptionInfo : subscriptionInfos) {

                    final String simSerialNumber = subscriptionInfo.getIccId();

                    // Return User Orders

                    String url = getString(R.string.api_url) + simSerialNumber + "&lat=" + myLatitude + "&long=" + myLongitude;
                    NetworkGetRequest networkGetRequest = new NetworkGetRequest();
                    networkGetRequest.run(url, new Callback() {
                      @Override
                      public void onFailure(Call call, IOException e) {
                        e.printStackTrace();
                      }

                      @Override
                      public void onResponse(Call call, Response response) throws IOException {
                        if (!response.isSuccessful()) {
                          throw new IOException("Unexpected code" + response);
                        }
                        final String responseData = response.body().string();
                        MainActivity.this.runOnUiThread(new Runnable() {
                          @Override
                          public void run() {
                            try {
                              ((ApplicationVars)getApplication()).setJSONObjectOrders(new JSONObject(responseData));

                                // Added Orders to list

                                orders = ((ApplicationVars)getApplication()).getJSONObjectOrders().getJSONObject("zaeavki");
                                String[] ordersID = new String[orders.length()];
                                String[] ordersDate = new String[orders.length()];
                                String[] ordersName = new String[orders.length()];
                                String[] clientsName = new String[orders.length()];
                                String[] clientsPhones = new String[orders.length()];
                                String[] techNames = new String[orders.length()];
                                String[] serialNumbers = new String[orders.length()];
                                String[] clientsAddress = new String[orders.length()];
                                String[] clientsDistance = new String[orders.length()];
                                for (int i = 0; i < orders.length(); i++) {

                                  final JSONObject order = orders.getJSONObject(String.valueOf(i));
                                  ((ApplicationVars) getApplication()).setOrderID(order.getString("id"));
                                  ((ApplicationVars) getApplication()).setOrderDate(order.getString("data"));
                                  ((ApplicationVars) getApplication()).setOrderName(order.getString("tip order"));
                                  ((ApplicationVars) getApplication()).setClientName(order.getString("client_name"));
                                  ((ApplicationVars) getApplication()).setClientPhone(order.getString("contact_nr"));
                                  ((ApplicationVars) getApplication()).setClientLatitude(order.getString("lat"));
                                  ((ApplicationVars) getApplication()).setClientLongitude(order.getString("long"));
                                  ((ApplicationVars) getApplication()).setTechName(order.getString("tehnica"));
                                  ((ApplicationVars) getApplication()).setSerialNumber(order.getString("shasi"));
                                  ((ApplicationVars) getApplication()).setClientDistance(order.getString("distance"));
                                  ((ApplicationVars) getApplication()).setClientDistance(order.getString("location_str"));

                                  ordersID[i] = ((ApplicationVars) getApplication()).getOrderID();
                                  ordersDate[i] = ((ApplicationVars) getApplication()).getOrderDate();
                                  ordersName[i] = ((ApplicationVars) getApplication()).getOrderName();
                                  clientsName[i] = ((ApplicationVars) getApplication()).getclientName();
                                  clientsPhones[i] = ((ApplicationVars) getApplication()).getClientPhone();
                                  techNames[i] = ((ApplicationVars) getApplication()).getTechName();
                                  serialNumbers[i] = ((ApplicationVars) getApplication()).getSerialNumber();
                                  clientsAddress[i] = ((ApplicationVars) getApplication()).getClientAddress();
                                  clientsDistance[i] = ((ApplicationVars) getApplication()).getClientDistance();
                                }
                                CustomMainList listItem = new CustomMainList(MainActivity.this, clientsName, clientsPhones, clientsAddress, clientsDistance, serialNumbers, ordersID, ordersDate, ordersName, techNames);
                                listOrders.setAdapter(listItem);

                                swipeRefreshLayout.setRefreshing(false);

                            } catch (JSONException e) {
                              e.printStackTrace();
                            }
                          }
                        });
                      }
                    });
                  }
                }
              }

              @Override
              public void onStatusChanged(String provider, int status, Bundle extras) {

              }

              @Override
              public void onProviderEnabled(String provider) {

              }

              @Override
              public void onProviderDisabled(String provider) {

              }
            };

            // Location Updater

            locationManager.requestLocationUpdates(locationManager.NETWORK_PROVIDER, 5 * 60 * 1000, 0, locationListener);

          }
        });

        // Personal Cabinet

        user = findViewById(R.id.user);

        listOrders = findViewById(R.id.listOrders);

        user.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            Intent userIntent = new Intent(MainActivity.this, UserActivity.class);
            startActivity(userIntent);
          }
        });

//        listOrders.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
//
//                TextView textView = view.findViewById(R.id.clientPhone);
//                TextView name = view.findViewById(R.id.clientName);
//                TextView addresstext = view.findViewById(R.id.clientAddress);
//                TextView serialInfo = view.findViewById(R.id.serialNumber);
//
//                serialNumber = serialInfo.getText().toString();
//                phone = textView.getText().toString();
//                address = addresstext.getText().toString();
//
//
//                final String orderIndex = name.getTag().toString();
//                try {
//                    final JSONObject order = orders.getJSONObject(orderIndex);
//                    final AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
//                    View v = getLayoutInflater().inflate(R.layout.custom_main_alert, null);
//                    Button call = v.findViewById(R.id.call);
//                    Button map = v.findViewById(R.id.map);
//                    Button info = v.findViewById(R.id.info);
//                    alert.setCancelable(true);
//                    alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                        }
//                    });
//                    alert.setView(v);
//                    alert.show();
//
//                    call.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            if (checkPermission(Manifest.permission.CALL_PHONE)) {
//                                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone)));
//                            } else {
//                                Toast.makeText(MainActivity.this, "Permission Call Phone denied", Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    });
//
//                    map.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//
//                            try {
//                                clientLatitude = order.getString("lat");
//                                clientLongitude = order.getString("long");
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            Uri gmmIntentUri = Uri.parse("http://maps.google.com/maps?saddr="+myLatitude+","+myLongitude+"&daddr="+clientLatitude+","+clientLongitude+"?z=12");//Uri.parse("geo:"+clientLatitude+","+clientLongitude+"?z=12&q"+address);//. + clientLatitude + "," + clientLongitude + "("+ address + ")");
//                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//                            mapIntent.setPackage("com.google.android.apps.maps");
//                            startActivity(mapIntent);
//                        }
//                    });
//                    info.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Intent info = new Intent(MainActivity.this, OrderActivity.class);
//                            info.putExtra("serialInfo", serialNumber);
//                            startActivity(info);
//                        }
//                    });
//                    if (checkPermission(Manifest.permission.CALL_PHONE)) {
//                    } else {
//                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CALL_PHONE}, 1);
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
    }

    private boolean checkPermission(String permission) {
        return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode) {
            case 1 :
                if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Toast.makeText(this, "You can call the number by clicking on the button", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }

    // Options Menu

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater menuInflater = getMenuInflater();
    menuInflater.inflate(R.menu.menu, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

      switch (item.getItemId()){
        case R.id.helpSettings:
          Toast.makeText(getApplicationContext(), "Help", Toast.LENGTH_SHORT).show();
          break;

        case R.id.updateSettings:
          Toast.makeText(getApplicationContext(), "Update", Toast.LENGTH_SHORT).show();
          break;

        case R.id.settings:
          Intent settingIntent = new Intent(MainActivity.this, SettingsActivity.class);
          startActivity(settingIntent);
          break;

      }

    return super.onOptionsItemSelected(item);
  }

  public void orderClick(View v){

      TextView idText = v.findViewById(R.id.idOrder);
      String id = idText.getText().toString();
      LinearLayout techInfo = findViewById(R.id.techInfo);
      String techName = null;
      String serial = null;

       int techInfoCount = techInfo.getChildCount();

       for (int i = 0; i < techInfoCount; i++){

         TextView techNameTextView = findViewById(R.id.techName);
         TextView serialNameTextView = findViewById(R.id.serialNumber);
         techName = techNameTextView.getText().toString();
         serial = serialNameTextView.getText().toString();
       }

      Intent orderActivity = new Intent(MainActivity.this, OrderActivity.class);
      orderActivity.putExtra("orderID", id);
      orderActivity.putExtra("techName", techName);
      orderActivity.putExtra("serial", serial);
      startActivity(orderActivity);
    }
    public void clientInfo(View v){
      View view = (View) v.getParent();

      View parentView = (View) view.getParent();

      TextView clientName = view.findViewById(R.id.clientName);
      String clientNameString = clientName.getText().toString();

      TextView clientPhone = view.findViewById(R.id.clientPhone);
      String clientPhoneString = clientPhone.getText().toString();

      TextView techName = parentView.findViewById(R.id.techName);
      String techNameString = techName.getText().toString();

      TextView serialNumber = parentView.findViewById(R.id.serialNumber);
      String serialNumberString = serialNumber.getText().toString();


      Intent clientIntent = new Intent(MainActivity.this, ClientActivity.class);
      clientIntent.putExtra("clientName", clientNameString);
      clientIntent.putExtra("clientPhone", clientPhoneString);
      clientIntent.putExtra("techName", techNameString);
      clientIntent.putExtra("techSerialNumber", serialNumberString);
      startActivity(clientIntent);
    }
    public void clientCall(View v){
      TextView phone = v.findViewById(R.id.clientPhone);
      String phoneString = phone.getText().toString();

      if (checkPermission(Manifest.permission.CALL_PHONE)) {
        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneString)));
      } else {
        Toast.makeText(MainActivity.this, "Permission Call Phone denied", Toast.LENGTH_SHORT).show();
      }
    }
    public void techInfoClick(View v){
      TextView techName = v.findViewById(R.id.techName);
      TextView serialNumber = v.findViewById(R.id.serialNumber);

      String techNameString = techName.getText().toString();
      String serialNumberString = serialNumber.getText().toString();

      Intent productIntent = new Intent(MainActivity.this, ActivityProduct.class);
      productIntent.putExtra("techName", techNameString);
      productIntent.putExtra("serialNumber",serialNumberString);
      startActivity(productIntent);
    }
}
