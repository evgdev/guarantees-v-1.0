package com.example.guarantees;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class ActivityProduct extends AppCompatActivity {

ListView ordersHistory;

String[] ordersID, ordersDate, ordersType, productsID, productsCode, productsDen, productsCount, productsPrice;
String orderID, orderDate, orderType, productID, productCode, productDen, productCount, productPrice;
JSONArray[] orderMarfa;

TextView techName, serialNumber;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_product2);
    ordersHistory = findViewById(R.id.techHistoryList);

    techName = findViewById(R.id.techName);
    serialNumber = findViewById(R.id.serialNumber);

    Bundle bundle = getIntent().getExtras();
    String techNameString = bundle.getString("techName");
    String serialNumberString = bundle.getString("serialNumber");

    techName.setText(techNameString);
    serialNumber.setText(serialNumberString);

    Bundle bundle1 = getIntent().getExtras();
    String clientTechName = bundle1.getString("techName");
    String clientSerialoNumber = bundle1.getString("serialNumber");

    techName.setText(clientTechName);
    serialNumber.setText(clientSerialoNumber);

    String url = "http://eugen.agro.md/api/return-orders-history.php";
    NetworkGetRequest networkGetRequest = new NetworkGetRequest();
    networkGetRequest.run(url, new Callback() {
      @Override
      public void onFailure(Call call, IOException e) {
        e.printStackTrace();
      }

      @Override
      public void onResponse(Call call, Response response) throws IOException {
        if (!response.isSuccessful()){
          throw new IOException("Unexpected code" + response);
        }
        final String responseData = response.body().string();
        ActivityProduct.this.runOnUiThread(new Runnable() {
          @Override
          public void run() {
            try {
              JSONObject resp = new JSONObject(responseData);
              if (resp.length() == 0){
                Toast.makeText(ActivityProduct.this, "Can't load data", Toast.LENGTH_SHORT).show();
              } else {
                JSONArray response = resp.getJSONArray("orders");
                ordersID = new String[response.length()];
                ordersDate = new String[response.length()];
                ordersType = new String[response.length()];

                orderMarfa = new JSONArray[response.length()];

                for (int i = 0; i<response.length(); i++) {
                  JSONObject order = response.getJSONObject(i);

                  orderID = order.getString("order_id");
                  orderDate = order.getString("date_order");
                  orderType = order.getString("type_order");

                  JSONArray products = order.getJSONArray("marfa");
                  orderMarfa[i] = products;

                  ordersID[i] = orderID;
                  ordersDate[i] = orderDate;
                  ordersType[i] = orderType;
                }
                ProductInfoList infoList = new ProductInfoList(ActivityProduct.this, ordersID, ordersDate, ordersType);
                ordersHistory.setAdapter(infoList);

                ordersHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                  @Override
                  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    JSONArray products = orderMarfa[ position ];

                    AlertDialog.Builder alert = new AlertDialog.Builder(ActivityProduct.this);
                    View v = getLayoutInflater().inflate(R.layout.custom_products_alert, null);
                    ListView productsList = v.findViewById(R.id.productsList);
                    productsID = new String[products.length()];
                    productsCode = new String[products.length()];
                    productsDen = new String[products.length()];
                    productsCount = new String[products.length()];
                    productsPrice = new String[products.length()];

                    for (int iterator = 0; iterator< products.length(); iterator++){
                      try {
                        JSONObject pr = products.getJSONObject(iterator);
                        productID = pr.getString("tov_id");
                        productCode = pr.getString("cod_marfa");
                        productDen = pr.getString("den_marfa");
                        productCount = pr.getString("cantitate");
                        productPrice = pr.getString("pret_marfa");

                        productsID[iterator] = productID;
                        productsCode[iterator] = productCode;
                        productsDen[iterator] = productDen;
                        productsCount[iterator] = productCount;
                        productsPrice[iterator] = productPrice;
                      } catch (JSONException e) {
                        e.printStackTrace();
                      }
                    }
                    ProductsHistoryList productsHistoryList = new ProductsHistoryList(ActivityProduct.this, productsID, productsCode, productsDen, productsCount, productsPrice);
                    productsList.setAdapter(productsHistoryList);

                    alert.setView(v);
                    alert.show();
                  }
                });
              }
            } catch (JSONException e) {
              e.printStackTrace();
            }
          }
        });
      }
    });

  }
}
